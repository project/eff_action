/**
 * @file
 * Applies EFF day of action banner settings.
 */

(function (drupalSettings) {

  'use strict';

  window.banner_config = drupalSettings.effAction.bannerConfig;

})(drupalSettings);
